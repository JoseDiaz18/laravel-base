<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert([
            'name' => 'Super User',
            'email' => 'joseangel191134@gmail.com',
            'password' => '$2y$10$Tr26nDj.QxMPcBi/qipov.yo.B4DzS4M17XacW0qRfI8nxNh/R6Ei',
            'created_at' => '2021-06-28 19:07:58.000000'
        ]);

        DB::table('users')->insert([
            'name' => 'Usuario',
            'email' => 'user@gmail.com',
            'password' => '$2y$10$XpiY21jEF5a8HTcL0z8P5ucfvGUimglWO9eRi4tJIjdDidc5HbmAS',
            'created_at' => '2021-06-28 19:27:58.000000'
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
